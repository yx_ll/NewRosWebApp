import Vue from 'vue'
import vuex from 'vuex'
Vue.use(vuex)
//获取store:this.$store.state.xxx
export default new vuex.Store({
    state:{
        batteryBg:"#409EFF",
        batteryStatus: null,
        authorizationPassword: '6022',
        authorization: false,//授权模态框
        showMap: true,//地图
        addMap: false,//添加地图
        stationList:[],//站点列表
        trajectoryList: [],//轨迹列表
        showLogo: true,//logo
        showRobotLocation: true,//机器人位置
        robotPosition: '',//机器人坐标
        wifiInfos: '',//wifi数据
        
        showLaser:false,//激光
        showGlobalPath: false,//全局
        showLocalPath: false,//局部
        showOutline: false,//轮廓
        showStation: false,//站点
        showTrack:false,//轨迹
        showDrap: false,//拖拽圆盘
        showUpdate: false,//更新
        showVoice: false,//音量调节
        nowMapName:'',//当前地图名称
        showMapList: false,//显示地图列表
        mapList: [],//地图列表
        delMap:false,//地图删除模态框
        delMapName:'',//要删除的地图名称
        edipMapModule: false,//编辑地图模态框
        disabledMoveMap: false,//地图移动
        locationEstimate: false,//位置估计
        
        stationModule:false,//站点详情模态框
        trackModule: false,//轨迹详情模态框
        stationDetail:Object,//站点详情数据
        stationType:'',//类型
        stationMode:'',//模式
        stationName:'',//站点名称
        trackDetail: '',//轨迹详情
        trackName: '',//轨迹名称
        stationEnough:'',//站点范围
        stationEndtime:'',//超时时间
        stationEdit: false,//站点编辑模态框
        deleteStation:false,//站点删除模态框
        deleteTrack: false,//轨迹删除模态框
        stationsModule: false,//站点列表模态框
        showAddStation:false,//添加站点表单
        showAddTrack: false,//添加轨迹表单
        tracksModule:false,//轨迹列表
        showWifiSetting:false,//wifi设置
        showMoreBtns:false,//更多按钮
        showDragModule: false,//拖拽
        scale:10,
        battery:100,//电量
        showMask:false,//遮罩
    },
    mutations:{
        switch_dialog(state){
            state.showDrap = state.showDrap?false:true;
            //你还可以在这里执行其他的操作改变state
        },

        //切换电池状态
        switchBatteryStatus(state,type){
            state.batteryStatus = type
        },

        //放大缩小切换logo
        checkLogo(state,type){
            state.showLogo = type
        },


        //设置wifi
        setWifiInfos(state,infos){
            state.wifiInfos = infos
        },

        //显示/隐藏设置权限
        switchAutherModule(state,type){
            state.authorization = type;
        },

        //显示/隐藏添加地图
        switchAddMap(state,type){
            state.addMap = type
        },

        //显示/隐藏位置估计
        switchLocationEstimate(state,type){
            state.locationEstimate = type;
        },

        //修改设置机器人坐标
        setRobotPosition(state,info){
            state.robotPosition = info;
        },

        //显示/隐藏更新
        switchUpdate(state, type){
            state.showUpdate = type
        },

        //当前地图名称
        setNowMapName(state,type){
            state.nowMapName = type;
        },

        //地图删除模态框
        switchDelMap(state,type){
            state.delMap = type;
        },

        //显示地图列表
        switchMapList(state,type){
            state.showMapList = type;
        },

        //设置地图列表数据
        setMapList(state,info){
            state.mapList = info;
        },

        //要删除的地图名称
        setDelMapName(state,info){
            state.delMapName = info;
        },

        //编辑地图按钮组模态框
        switchEditMapBtns(state,type){
            state.edipMapModule = type;
        },

        //禁止和开启移动地图
        switchMoveMap(state,type){
            state.disabledMoveMap = type;
        },

        //站点列表list
        stationList(state, type = []){
            state.stationList = type
        },

        //轨迹列表
        trajectoryList(state, type = []){
            state.trajectoryList = type;
        },

        //站点详情模态框
        switchStationModule(state, type){
            state.stationModule = type;
        },

        //站点详情数据
        setStationDetail(state,info){
            state.stationDetail = info;
        },

        //站点类型
        setStationType(state,type = ''){
            state.stationType = type;
        },

        //站点模式
        setStationMode(state,type = ''){
            state.stationMode = type;
        },

        //站点范围
        setStationEnough(state,type = ''){
            state.stationEnough = type
        },

        //超时时间
        setStationEndtime(state,type=''){
            state.stationEndtime = type;
        },

        //站点编辑
        switchStationEdit(state,type){
            state.stationEdit = type
        },
        //站点删除
        switchDeleteStation(state,type){
            state.deleteStation = type
        },

        //设置轨迹详情
        setTrackDetail(state,info){
            state.trackDetail = info
        },

        //轨迹删除
        switchDeleteTrack(state,type){
            state.deleteTrack = type;
        },

        //站点名称赋值
        setStationName(state,msg = ""){
            state.stationName = msg;
        },

        //轨迹名称赋值
        setTrackName(state,msg = ""){
            state.trackName = msg;
        },

        //显示/隐藏添加站点列表
        switchStationList(state, type){
            state.stationsModule = type
        },

        //显示/隐藏添加站点
        switchAddStation(state, type){
            state.showAddStation = type
        },

        //显示/隐藏添加轨迹
        switchAddTrack(state,type){
            state.showAddTrack = type;
        },

        //显示/隐藏添加轨迹列表
        switchTrackList(state,type){
            state.tracksModule = type;
        },

        //显示/隐藏轨迹详情模态框
        switchTrackModule(state,type){
            state.trackModule = type;
        },

        //切换wifi设置
        switchWifiSetting(state,type){
            state.showWifiSetting = type
        },

        //切换更多按钮
        switchMoreBtns(state,type = false){
            state.showMoreBtns = type
        },

        //切换音量调节
        switchVoice(state,type){
            state.showVoice = type
        },

        //切换拖拽模态框
        switchDragModule(state,type = false){
            state.showDragModule = type
        },

        //放大缩小比例
        mapScale(state,type){
            state.scale = type
        },

        //电量
        subButtery(state,type){
            state.battery = type
        },

        //电量颜色
        setBatteryBg(state,type){
            state.batteryBg = type
        },

        //显示遮罩
        switchShowMask(state,type){
            state.showMask = type
        }
    }
})